use std::collections::HashSet;

#[derive(Debug, PartialEq)]
struct Bag {
    first: HashSet<char>,
    second: HashSet<char>,
}
impl Bag {
    fn new() -> Bag {
        Bag {
            first: HashSet::new(),
            second: HashSet::new(),
        }
    }
    fn items(&self) -> HashSet<char> {
        self.first.union(&self.second).map(|&c| c).collect()
    }
    fn in_both_sides(&self) -> &char {
        self.first
            .intersection(&self.second)
            .next()
            .expect("il n’en restera qu’un")
    }
    fn commons_with(&self, other: &Bag) -> HashSet<char> {
        self.items()
            .intersection(&other.items())
            .map(|&c| c)
            .collect()
    }
}
fn bag_from(line: &str) -> Bag {
    let mut bag = Bag::new();
    line.chars().enumerate().for_each(|(i, c)| {
        if i < line.len() / 2 {
            bag.first.insert(c);
        } else {
            bag.second.insert(c);
        }
    });
    bag
}

fn score(c: &char) -> usize {
    match c {
        'a' => 1,
        'b' => 2,
        'c' => 3,
        'd' => 4,
        'e' => 5,
        'f' => 6,
        'g' => 7,
        'h' => 8,
        'i' => 9,
        'j' => 10,
        'k' => 11,
        'l' => 12,
        'm' => 13,
        'n' => 14,
        'o' => 15,
        'p' => 16,
        'q' => 17,
        'r' => 18,
        's' => 19,
        't' => 20,
        'u' => 21,
        'v' => 22,
        'w' => 23,
        'x' => 24,
        'y' => 25,
        'z' => 26,
        c => {
            // ça doit être un caractère en majuscule
            26 + score(
                &c.to_lowercase()
                    .next()
                    .expect("normalement, on n’a que des caractères ACSII"),
            )
        }
    }
}

#[aoc_generator(day3)]
fn parse_d3_p1(input: &str) -> Vec<Bag> {
    input.lines().map(|line| bag_from(&line)).collect()
}

#[aoc(day3, part1)]
fn solve_d3_p1(input: &[Bag]) -> usize {
    input.iter().map(|b| score(&b.in_both_sides())).sum()
}

#[aoc(day3, part2)]
fn solve_d3_p2(input: &Vec<Bag>) -> usize {
    input
        .chunks(3)
        .map(|chunk| match chunk {
            [bag1, bag2, bag3] => score(
                bag1.commons_with(&bag2)
                    .intersection(&bag3.items())
                    .next()
                    .expect("il doit y en avoir un"),
            ),
            _ => unreachable!("arf !"),
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

    #[test]
    fn test_parse() {
        let input = parse_d3_p1(&TEST_INPUT);
        let exp_bag = Bag {
            first: HashSet::from_iter("vJrwpWtwJgWr".chars()),
            second: HashSet::from_iter("hcsFMMfFFhFp".chars()),
        };
        assert_eq!(input[0], exp_bag);
    }

    #[test]
    fn test_part1() {
        let i = parse_d3_p1(&TEST_INPUT);
        assert_eq!(solve_d3_p1(&i), 157);
    }

    #[test]
    fn test_part2() {
        let i = parse_d3_p1(&TEST_INPUT);
        assert_eq!(solve_d3_p2(&i), 70);
    }
}
