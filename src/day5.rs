#[derive(Debug, PartialEq)]
struct Action {
    from: usize,
    to: usize,
    times: usize,
}
impl Action {
    fn from_line(s: &str) -> Self {
        let parts: Vec<&str> = s.split(" ").collect();
        let times: usize = parts[1].parse().unwrap();
        let from: usize = parts[3].parse().unwrap();
        let to: usize = parts[5].parse().unwrap();
        Action { times, from, to }
    }
}

type Stack = Vec<char>;

struct Day5Input {
    stacks: Vec<Stack>,
    actions: Vec<Action>,
}

fn rotate_right<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|row| row.into_iter()).rev().collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|iter| iter.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

#[aoc_generator(day5)]
fn parse(input: &str) -> Day5Input {
    // collect the stacks
    let mut input = input.lines();

    let raw_stacks: Vec<Vec<char>> = rotate_right(
        input
            .by_ref()
            .take_while(|line| line.len() > 0)
            .map(|line| line.chars().skip(1).collect::<Vec<char>>())
            .collect(),
    );
    let stacks = raw_stacks
        .into_iter()
        // keep only stacks, identified by their index
        .filter(|seq| seq.len() > 0 && seq[0].is_numeric())
        .map(|seq| {
            seq[1..] // skip head
                .into_iter()
                .filter(|c| c.is_alphabetic()) // remove trailing whitespaces
                .map(|&c| c)
                .collect()
        })
        .collect();

    let actions = input.map(|line| Action::from_line(line)).collect();
    Day5Input { stacks, actions }
}

fn tops(stacks: &Vec<Stack>) -> String {
    stacks
        .iter()
        .map(|stack| stack.last().expect("pas de stack vide").clone())
        .collect()
}

#[aoc(day5, part1)]
fn solve_d5_p1(input: &Day5Input) -> String {
    let mut stacks: Vec<Stack> = input.stacks.clone();
    for action in &input.actions {
        for _ in 0..action.times {
            // taking care that stacks indices are 1-based
            let c = stacks[action.from - 1]
                .pop()
                .expect("il y a un conteneur à bouger")
                .clone();
            stacks[action.to - 1].push(c);
        }
    }
    // eprintln!("stacks: {:?}", stacks);
    tops(&stacks)
}

#[aoc(day5, part2)]
fn solve_d5_p2(input: &Day5Input) -> String {
    let mut stacks: Vec<Stack> = input.stacks.clone();
    for action in &input.actions {
        // taking care that stacks indices are 1-based
        let pile_start_idx = stacks[action.from - 1].len() - action.times;
        let pile: Stack = stacks[action.from - 1][pile_start_idx..]
            .iter()
            .map(|&c| c)
            .collect();
        stacks[action.to - 1].extend(pile);
        stacks[action.from - 1].truncate(pile_start_idx);
    }
    tops(&stacks)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
";
    const EXP_SOLUTION: &str = "CMZ";
    const EXP_SOLUTION_P2: &str = "MCD";

    #[test]
    fn test_transpose() {
        let i: Vec<Vec<char>> = vec!["ab ".chars().collect(), "def".chars().collect()];
        let exp: Vec<Vec<char>> = vec![vec!['d', 'a'], vec!['e', 'b'], vec!['f', ' ']];
        assert_eq!(rotate_right(i), exp);
    }

    #[test]
    fn test_parse() {
        let input = parse(&INPUT);
        assert_eq!(
            input.stacks,
            vec![vec!['Z', 'N'], vec!['M', 'C', 'D'], vec!['P']]
        );
        assert_eq!(
            input.actions[0],
            Action {
                from: 2,
                to: 1,
                times: 1,
            }
        );
        assert_eq!(
            input.actions[1],
            Action {
                from: 1,
                to: 3,
                times: 3,
            }
        );
    }

    #[test]
    fn test_solve_part1() {
        let input: Day5Input = parse(&INPUT);
        let exp: String = String::from(EXP_SOLUTION);
        assert_eq!(solve_d5_p1(&input), exp);
    }

    #[test]
    fn test_solve_part2() {
        let input: Day5Input = parse(&INPUT);
        let exp: String = String::from(EXP_SOLUTION_P2);
        assert_eq!(solve_d5_p2(&input), exp);
    }
}
