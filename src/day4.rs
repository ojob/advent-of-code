use std::cmp::min;

#[derive(Debug, PartialEq)]
struct Range {
    start: usize,
    end: usize,
}
impl Range {
    /// Returns true if there is coverage of one by another
    fn is_mutual_cover(&self, other: &Range) -> bool {
        (self.start <= other.start && self.end >= other.end)
            || (other.start <= self.start && other.end >= self.end)
    }
    /// Indicate how much overlap there is
    fn overlap(&self, other: &Range) -> usize {
        if self.is_mutual_cover(&other) {
            min(self.end - self.start + 1, other.end - other.start + 1)
        } else if self.start <= other.start && self.end >= other.start {
            self.end - other.start + 1
        } else if other.start <= self.start && other.end >= self.start {
            other.end - self.start + 1
        } else {
            0
        }
    }
    fn from_str(s: &str) -> Self {
        let mut parts = s.split("-");
        Range {
            start: parts
                .next()
                .expect("au moins un item")
                .parse()
                .expect("qui doit être un entier non signé"),
            end: parts
                .next()
                .expect("au moins deux items")
                .parse()
                .expect("qui doit être un entier non signé"),
        }
    }
}

#[aoc_generator(day4)]
fn parse(input: &str) -> Vec<[Range; 2]> {
    input
        .lines()
        .map(|line| {
            let mut parts = line.split(",");
            [
                Range::from_str(parts.next().expect("au moins un range")),
                Range::from_str(parts.next().expect("au moins deux ranges")),
            ]
        })
        .collect()
}

#[aoc(day4, part1)]
fn solve_d4_p1(input: &[[Range; 2]]) -> usize {
    input
        .iter()
        .map(|[first, second]| if first.is_mutual_cover(&second) { 1 } else { 0 })
        .sum()
}

#[aoc(day4, part2)]
fn solve_d4_p2(input: &[[Range; 2]]) -> usize {
    input
        .iter()
        .map(|[first, second]| if first.overlap(&second) > 0 { 1 } else { 0 })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

    #[test]
    fn test_day4_part1() {
        let input: Vec<[Range; 2]> = parse(&INPUT);
        // unit test towards part 1
        assert_eq!(
            input[0],
            [Range { start: 2, end: 4 }, Range { start: 6, end: 8 }]
        );
        // test part 1
        assert_eq!(solve_d4_p1(&input), 2);

        // unit test towards part 2
        assert_eq!(input[0][0].overlap(&input[0][1]), 0);
        assert_eq!(input[2][0].overlap(&input[2][1]), 1);
        assert_eq!(input[3][0].overlap(&input[3][1]), 5);
        assert_eq!(input[5][0].overlap(&input[5][1]), 3);
        // test part 2
        assert_eq!(solve_d4_p2(&input), 4);
    }
}
