use std::collections::HashSet;

fn solve(input: &[char], size: usize) -> usize {
    assert!(input.len() >= size);
    for i in (size - 1)..input.len() {
        let uniques: HashSet<char> =
            HashSet::from_iter(input[(i + 1 - size)..=i].iter().map(|&c| c));
        if uniques.len() == size {
            return i + 1;
        };
    }
    0
}

#[aoc_generator(day6)]
fn parse(input: &str) -> Vec<char> {
    input.chars().collect()
}

#[aoc(day6, part1)]
fn solve_d6_p1(input: &[char]) -> usize {
    solve(&input, 4)
}

#[aoc(day6, part2)]
fn solve_d6_p2(input: &[char]) -> usize {
    solve(&input, 14)
}

#[cfg(test)]
mod tests {
    use super::*;

    const SAMPLES: [(&str, usize, usize); 5] = [
        ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7, 19),
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5, 23),
        ("nppdvjthqldpwncqszvftbrmjlhg", 6, 23),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10, 29),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11, 26),
    ];

    #[test]
    fn test_solve_d6_p1() {
        for (input, exp, _) in SAMPLES.iter() {
            eprintln!("attempt to solve {:?}", input);
            let input = parse(input);
            assert_eq!(solve_d6_p1(&input), *exp);
        }
    }

    #[test]
    fn test_solve_d6_p2() {
        for (input, _, exp) in SAMPLES.iter() {
            eprintln!("attempt to solve {:?}", input);
            let input = parse(input);
            assert_eq!(solve_d6_p2(&input), *exp);
        }
    }
}
