// ---- data model ------------------------------------------------------------
pub enum GameResult {
    Win,
    Draw,
    Loss,
}
impl GameResult {
    fn score(&self) -> usize {
        match self {
            GameResult::Win => 6,
            GameResult::Draw => 3,
            GameResult::Loss => 0,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RPS {
    Rock,
    Paper,
    Scissors,
}
impl RPS {
    fn score(&self) -> usize {
        match &self {
            RPS::Rock => 1,
            RPS::Paper => 2,
            RPS::Scissors => 3,
        }
    }
    fn play(&self, other: &RPS) -> GameResult {
        match (self, other) {
            (RPS::Rock, RPS::Scissors) | (RPS::Scissors, RPS::Paper) | (RPS::Paper, RPS::Rock) => {
                GameResult::Win
            }
            (a, b) if a == b => GameResult::Draw,
            _ => GameResult::Loss,
        }
    }
    /// Given current move and expected game result, what is the move to perform?
    fn choose(&self, result: &GameResult) -> RPS {
        match result {
            GameResult::Win => match self {
                RPS::Rock => RPS::Paper,
                RPS::Paper => RPS::Scissors,
                RPS::Scissors => RPS::Rock,
            },
            GameResult::Draw => *self,
            GameResult::Loss => match self {
                RPS::Rock => RPS::Scissors,
                RPS::Paper => RPS::Rock,
                RPS::Scissors => RPS::Paper,
            },
        }
    }
}
pub fn rps_from_letter(l: &str) -> RPS {
    match l {
        "A" | "X" => RPS::Rock,
        "B" | "Y" => RPS::Paper,
        "C" | "Z" => RPS::Scissors,
        &_ => unreachable!("unknown action!"),
    }
}
pub fn gameres_from_letter(l: &str) -> GameResult {
    match l {
        "X" => GameResult::Loss,
        "Y" => GameResult::Draw,
        "Z" => GameResult::Win,
        &_ => unreachable!("unknown action!"),
    }
}

// ---- inputs parsers --------------------------------------------------------
#[aoc_generator(day2, part1)]
pub fn parse_d02p1(input: &str) -> Vec<(RPS, RPS)> {
    input
        .lines()
        .map(|line| {
            let mut letters = line.split(" ");
            (
                rps_from_letter(letters.next().unwrap()),
                rps_from_letter(letters.next().unwrap()),
            )
        })
        .collect()
}

#[aoc_generator(day2, part2)]
pub fn parse_d02p2(input: &str) -> Vec<(RPS, GameResult)> {
    input
        .lines()
        .map(|line| {
            let mut letters = line.split(" ");
            (
                rps_from_letter(letters.next().unwrap()),
                gameres_from_letter(letters.next().unwrap()),
            )
        })
        .collect()
}

// ---- solvers ---------------------------------------------------------------
#[aoc(day2, part1)]
pub fn solve_d02p1(input: &[(RPS, RPS)]) -> usize {
    input
        .iter()
        .map(|(opponent, mine)| mine.play(opponent).score() + mine.score())
        .sum()
}

#[aoc(day2, part2)]
pub fn solve_d02p2(input: &[(RPS, GameResult)]) -> usize {
    input
        .iter()
        .map(|(opponent_rps, game_res)| {
            let mine = opponent_rps.choose(game_res);
            mine.score() + game_res.score()
        })
        .sum()
}

// ---- tests -----------------------------------------------------------------
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_rps() {
        assert_eq!(RPS::Paper, rps_from_letter("B"))
    }

    #[test]
    fn test_day1_part1() {
        let in_ = "A Y\nB X\nC Z\n";
        let input = parse_d02p1(in_);
        assert_eq!(solve_d02p1(&input), 15);
    }

    #[test]
    fn test_day2_part2() {
        let in_ = "A Y\nB X\nC Z\n";
        let input = parse_d02p2(in_);
        assert_eq!(solve_d02p2(&input), 12);
    }
}
