#[aoc_generator(day1)]
pub fn parse_d01(input: &str) -> Vec<usize> {
    input.lines().fold(vec![0], |mut acc, l| {
        let last: usize = acc.len() - 1;
        match l {
            "" => {
                acc.push(0);
                acc
            }
            l => {
                acc[last] += l.trim().parse::<usize>().unwrap();
                acc
            }
        }
    })
}

#[aoc(day1, part1)]
pub fn solve_d01p1(input: &[usize]) -> usize {
    eprintln!("input: {:?}", input);
    *input.iter().max().unwrap()
}

#[aoc(day1, part2)]
pub fn solve_d01p2(input: &[usize]) -> usize {
    let mut stock: Vec<usize> = input.iter().map(|&v| v).collect();
    stock.sort_unstable(); // quicker than the usual 'sort'
    stock.into_iter().rev().take(3).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day1_part2() {
        let in_ = "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000\n";
        let input = parse_d01(in_);
        assert_eq!(solve_d01p2(&input), 45000);
    }
}
