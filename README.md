# Advent Of Code 2022

Mes solutions

[Tableau de scores Kaizen](https://adventofcode.com/2022/leaderboard/private/view/796831)

## Utiliser ce projet

Ce projet s’appuie sur [cargo-aoc](https://github.com/gobanos/cargo-aoc) pour récupérer les données de jeu et afficher les résultats des solutions au fur et à mesure qu’elles sont codées.

    // installation
    cargo install cargo-aoc

    // utilisation
    cargo aoc

Les tests se lancent comme d’habitude :

    cargo test

